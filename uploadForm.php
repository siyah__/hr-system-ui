<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="cvform.css">
    <link rel="stylesheet" href="file1.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php
session_start();
$conn = new mysqli("localhost", "root", "", "hr_system");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$email = $_GET['email'];
?>

</head>
    <header>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Upload File</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </header>
    <body>
        <div class="container">         
            <div class="page-header">
                <h1>Upload Your Files : <small><b>Upload files by selecting all of them at once</b></small> </h1>
                <h4>Files we need : <small><b><ul><li> Certified ID/Passport </li><li> Academic Transcripts </li><li> Disability proof (If applicable) </li><li> CV (Optional) </li><li> Proof of residence </li></ul></b></small> </h4>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="POST" enctype="multipart/form-data" name="formUploadFile" id="uploadForm" action="upload.php">

                        <div class="row">
                         <div class="col-lg-6"> 
                             <div class= column  id = "emailContainer">
                                <div class="formLabel">
                                    <label>Email <span class="mustFillin">* </span>
                                        </label>
                                </div>
                
                                <div class="FormInput">
                                    <input value="<?php echo $email ?>"  id="reg-email"  type="email" name="email" required>
                                </div>
                
                            </div>
                        </div>
                    </div>
                    <hr>
                    <br>
                    

                        <div class="form-group">
                            <label for="exampleInputFile">Select files to upload:</label>
                            <input type="file" id="exampleInputFile" name="files[]" multiple="multiple">
                            <p class="help-block"><span class="label label-info">Note:</span> Please, Select the only (.jpg, .jpeg, .png, .gif , .pdf) to upload with the size of 3MB(s) only.</p>
                        </div>          
                        <button type="submit" class="btn btn-primary" name="btnSubmit" >Start Upload</button>
                        <a href="view.php?email=<?php echo $email ?>" class="btn btn-info">Show Uploaded Files</a>
                    </form>
                    <br/>
                    <label for="Progressbar">Progress:</label>
                    <div class="progress" id="Progressbar">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id="divProgressBar">
                            <span class="sr-only">45% Complete</span>
                        </div>                      
                    </div>
                    <div id="status">
                    </div>
                    <div><a href="SkillForm.php?email=<?php echo $email ?>" class="btn btn-info"><u>Next Step</u></a></div>
                </div>
            </div>
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jQuery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
        <script src="js/jQuery.Form.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){           
                
                var divProgressBar=$("#divProgressBar");
                var status=$("#status");
                
                $("#uploadForm").ajaxForm({
                    
                    dataType:"json",
                    
                    beforeSend:function(){
                        divProgressBar.css({});
                        divProgressBar.width(0);
                    },
                    
                    uploadProgress:function(event, position, total, percentComplete){
                        var pVel=percentComplete+"%";
                        divProgressBar.width(pVel);
                    },
                    
                    complete:function(data){
                        status.html(data.responseText);
                    }
                });
            });
        </script>
    </body>
</html>