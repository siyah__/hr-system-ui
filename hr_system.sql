-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 28, 2019 at 12:57 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_system`
--
CREATE DATABASE IF NOT EXISTS `hr_system` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `hr_system`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `email`, `user_type`, `password`) VALUES
('Siya', 'siyavuya@gmail.com', 'admin', '4297f44b13955235245b2497399d7a93'),
('sarah', 'sarah@testhr.com', 'HR', 'ec26202651ed221cf8f993668c459d46'),
('Jose', 'trocadoj@gmail.com', 'HR', '4297f44b13955235245b2497399d7a93');

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
CREATE TABLE IF NOT EXISTS `application` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  PRIMARY KEY (`application_id`),
  KEY `user_email` (`user_email`),
  KEY `vacancy_id` (`vacancy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`application_id`, `user_email`, `vacancy_id`) VALUES
(26, 'russ@gmail.com', 4),
(27, 'ngcobo@gmail.com', 6),
(28, 'ngcobo@gmail.com', 1),
(29, 'ngcobo@gmail.com', 1),
(30, 'tien@gmail.com', 1),
(31, 'tien@gmail.com', 1),
(32, 'tien@gmail.com', 1),
(33, 'tien@gmail.com', 1),
(34, 'tien@gmail.com', 1),
(35, 'tien@gmail.com', 1),
(36, 'tien@gmail.com', 1),
(37, 'tien@gmail.com', 1),
(38, 'tien@gmail.com', 1),
(39, 'tien@gmail.com', 1),
(40, 'tien@gmail.com', 1),
(41, 'tien@gmail.com', 1),
(42, 'tien@gmail.com', 1),
(43, 'tien@gmail.com', 1),
(44, 'tien@gmail.com', 1),
(45, 'tien@gmail.com', 1),
(46, 'tien@gmail.com', 1),
(47, 'tien@gmail.com', 1),
(48, 'tien@gmail.com', 1),
(49, 'tien@gmail.com', 1),
(50, 'tien@gmail.com', 1),
(51, 'tien@gmail.com', 1),
(52, 'tien@gmail.com', 1),
(53, 'trocadoj@gmail.com', 1),
(54, 'trocadoj@gmail.com', 1),
(55, 'trocadoj@gmail.com', 1),
(56, 'trocadoj@gmail.com', 1),
(57, 'trocadoj@gmail.com', 1),
(58, 'trocadoj@gmail.com', 1),
(59, 'trocadoj@gmail.com', 1),
(60, 'trocadoj@gmail.com', 1),
(61, 'trocadoj@gmail.com', 1),
(62, 'trocadoj@gmail.com', 1),
(63, 'trocadoj@gmail.com', 1),
(64, 'trocadoj@gmail.com', 1),
(65, 'tien@gmail.com', 1),
(66, 'tedy@gmail.com', 1),
(67, 'tedy@gmail.com', 1),
(68, 'tedy@gmail.com', 1),
(69, 'tedy@gmail.com', 1),
(70, 'tedy@gmail.com', 1),
(71, 'tedytinga@hotmail.com', 1),
(72, 'tedytinga@hotmail.com', 1),
(73, 'tedytinga@hotmail.com', 1),
(74, 'trocadoj@gmail.com', 1),
(75, 'trocadoj@gmail.com', 1),
(76, 'jenny@gmail.com', 1),
(77, 'jenny@gmail.com', 1),
(78, 'jenny@gmail.com', 1),
(79, 'jenny@gmail.com', 1),
(80, 'jenny@gmail.com', 1),
(81, 'miltonj@gmail.com', 1),
(82, 'aussy@gmail.com', 2),
(83, 'aussy@gmail.com', 2),
(84, 'aussy@gmail.com', 2),
(85, 'aussy@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `memberID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `resetToken` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `resetComplete` varchar(3) COLLATE utf8mb4_bin DEFAULT 'No',
  PRIMARY KEY (`memberID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`memberID`, `username`, `password`, `email`, `active`, `resetToken`, `resetComplete`) VALUES
(1, 'Aayush', '$2y$10$uLeusGcLYseRufFLxOu0p.CK7ryybwhXN/qvt369vKZQMyR9H0fi6', 'thakoraayush14@gmail.com', 'Yes', NULL, 'No'),
(2, 'xavier', '$2y$10$qdwCVp8T7VMnH2bDBZkj3OdXWRoPCzkf4/tiyU8q4dm6.DTtwd.au', 'xaver@gmail.com', 'Yes', NULL, 'No'),
(3, 'Kion', '$2y$10$T/RF7coxlFkye7lfqwQT1uHpzd2HPnCWzdFi2NNijP0MyIZ58Q/QK', 'Kion@gmail.com', 'Yes', NULL, 'No'),
(4, 'Gugu', '$2y$10$kuC0Qr3wT4SG.gJfEsWlvuPBFKcm3XJOztfXpoXc7uh6lijH6wMGy', 'Ngcobo@gmail.com', 'Yes', NULL, 'No'),
(5, 'Tien', '$2y$10$UrhOPpGcv/2YO6smsTABBe4eIcHzQNshpdvEnKcklIQDRrFa8C4o.', 'tien@gmail.com', 'Yes', NULL, 'No'),
(6, 'Tedy', '$2y$10$NiApLnsKWVw0E3VuGUViX.RDfqf/SBpGa5uSY/jJm8RxSw/HCgLIq', 'tedy@gmail.com', 'Yes', NULL, 'No'),
(7, 'Jenny', '$2y$10$qw.vE4tV33ab15yXRGnwoOTqIGQE9o.uuaGERaFdjs2gutFh1C8j2', 'jenny@gmail.com', 'Yes', NULL, 'No'),
(8, 'Milton', '$2y$10$TeRey4btvPR9/osbqsL3x.9Z1jlaTXPm0OocLPz.1Pt7rxN2kfjfC', 'miltonj@gmail.com', 'Yes', NULL, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `ranking`
--

DROP TABLE IF EXISTS `ranking`;
CREATE TABLE IF NOT EXISTS `ranking` (
  `ranking_id` int(11) NOT NULL AUTO_INCREMENT,
  `points` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `vacancy_id` int(11) NOT NULL,
  `kill_decision` varchar(5) NOT NULL,
  `salary_points` int(5) DEFAULT NULL,
  `education_points` int(5) DEFAULT NULL,
  `experience_points` int(5) DEFAULT NULL,
  `ethnicity_points` int(5) DEFAULT NULL,
  `skills_points` int(5) DEFAULT NULL,
  PRIMARY KEY (`ranking_id`),
  KEY `vacancy_id` (`vacancy_id`),
  KEY `user_emails` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ranking`
--

INSERT INTO `ranking` (`ranking_id`, `points`, `email`, `vacancy_id`, `kill_decision`, `salary_points`, `education_points`, `experience_points`, `ethnicity_points`, `skills_points`) VALUES
(14, 19, 'russ@gmail.com', 1, 'No', 5, 4, 2, 1, 7),
(15, 19, 'siya@gmail.com', 1, 'No', 5, 5, 7, 1, 1),
(16, 25, 'thakoraayush14@gmail.com', 1, 'No', 5, 3, 7, 8, 2),
(17, 40, 'jenny@gmail.com', 1, 'No', 10, 5, 5, 10, 10),
(18, 17, 'ngcobo@gmail.com', 6, 'No', 2, 6, 7, 2, 0),
(19, 20, 'ngcobo@gmail.com', 1, 'No', 5, 6, 7, 2, 0),
(20, 21, 'tedy@gmail.com', 1, 'No', 7, 8, 5, 1, 0),
(21, 16, 'tedytinga@hotmail.com', 1, 'No', 7, 1, 7, 1, 0),
(22, 19, 'miltonj@gmail.com', 1, 'No', 7, 6, 5, 1, 0),
(23, 19, 'russ@gmail.com', 4, 'No', 5, 4, 2, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `skill_level`
--

DROP TABLE IF EXISTS `skill_level`;
CREATE TABLE IF NOT EXISTS `skill_level` (
  `skill_level_name` varchar(255) NOT NULL,
  PRIMARY KEY (`skill_level_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill_level`
--

INSERT INTO `skill_level` (`skill_level_name`) VALUES
('Advanced'),
('Amateur'),
('Beginner'),
('Expert'),
('Intermediate');

-- --------------------------------------------------------

--
-- Table structure for table `userfiles`
--

DROP TABLE IF EXISTS `userfiles`;
CREATE TABLE IF NOT EXISTS `userfiles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `FilePath` varchar(250) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userfiles`
--

INSERT INTO `userfiles` (`ID`, `email`, `FilePath`, `FileName`) VALUES
(9, 'tedy@gmail.com', 'Upload', '216022279_TROCADO_ASS2.pdf'),
(10, 'tedy@gmail.com', 'Upload', '216022279_TROCADO_ASS4.pdf'),
(11, 'tedy@gmail.com', 'Upload', 'Business Analysis 3B Assignment.pdf'),
(12, 'tedy@gmail.com', 'Upload', 'CMN03B1 - Assignment #4 - Due on 18.10.2019.pdf'),
(13, 'tedy@gmail.com', 'Upload', '52474.jpg'),
(14, 'miltonj@gmail.com', 'Upload', 'Academic Transcript.pdf'),
(15, 'miltonj@gmail.com', 'Upload', 'ID.pdf'),
(16, 'miltonj@gmail.com', 'Upload', 'Milton CV.pdf'),
(17, 'miltonj@gmail.com', 'Upload', 'Proof of Residence.pdf'),
(18, 'aussy@gmail.com', 'Upload', 'Capture.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `contant_number` varchar(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `dob` varchar(15) NOT NULL,
  `id_number` varchar(13) NOT NULL,
  `citizenship` varchar(30) NOT NULL,
  `disabled` varchar(10) NOT NULL,
  `disability` varchar(10) DEFAULT NULL,
  `gender` varchar(8) NOT NULL,
  `highest_education_level` varchar(1000) NOT NULL,
  `ethnicity` varchar(10) NOT NULL,
  `job_title` varchar(500) NOT NULL,
  `years_of_experience` int(50) NOT NULL,
  `industry` varchar(50) NOT NULL,
  `relocate` varchar(50) NOT NULL,
  `region` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `min_salary` int(11) NOT NULL,
  `max_salary` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `appointment_type` varchar(50) NOT NULL,
  `employment_level` varchar(50) NOT NULL,
  `availability` varchar(50) NOT NULL,
  `lastest_employment` varchar(50) NOT NULL,
  `twitter_handle` varchar(50) DEFAULT NULL,
  `linkedin_profile` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_emails` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `email`, `contant_number`, `name`, `surname`, `dob`, `id_number`, `citizenship`, `disabled`, `disability`, `gender`, `highest_education_level`, `ethnicity`, `job_title`, `years_of_experience`, `industry`, `relocate`, `region`, `province`, `city`, `min_salary`, `max_salary`, `payment_type`, `appointment_type`, `employment_level`, `availability`, `lastest_employment`, `twitter_handle`, `linkedin_profile`) VALUES
(1, 'siya@gmail.com', '0715548846', 'Siyavuya', 'Viteka', '19970514', '', 'South African', 'No', 'None', 'Male', 'Diploma', 'Black', 'Database Admin', 1, 'IT', 'No', 'Gauteng', 'Gauteng', 'Joahnnesburg', 7000, 20000, 'Monthly', 'Full-time', 'Junior', 'Immediately', 'Absa', 'sssHjs', 'Siyavuya Viteka'),
(2, 'jenny@gmail.com', '0625318244', 'Jenny', 'smithy', '1998-11-10', '9811205186953', 'South African', 'No', 'none', 'Male', 'Honours Degree', 'Indian', 'Unemployed', 1, 'IT', 'No', 'greymont', 'Gauteng', 'jozi', 10000, 15000, 'Monthly', 'Full-Time', 'Junior', '0-1 Week', 'none', 'null', 'null'),
(3, 'thakoraayush14@gmail.com', '0618981252', 'Aayush', 'Thakor', '2019-10-09', '9905145862083', 'South African', 'No', 'Null', 'Male', 'Grade 9', 'Asian', 'Employed', 0, 'Automotive', 'Yes', 'Gauteng', 'KwaZulu-Natal', 'Johannesburg', 10000, 25000, 'Monthly', 'Full-Time', 'Graduate', '0-1 Week', 'FNB', 'Aayush Thakor', 'Aayush Thakor'),
(4, 'russ@gmail.com', '0787787787', 'Paul', 'Rest', '1986-04-30', '8607078976767', 'South African', 'No', 'None ', 'Male', 'Grade 12 (National Senior Certificate) or National (vocational) Cert. level 4', 'Coloured', 'Unemployed', 1, 'Professions', 'No', 'Ridgeway', 'Gauteng', 'Johannesburg', 5000, 20000, 'Weekly', 'Full-Time', 'Intermediate', '0-1 Week', 'fnb', 'zz', 'pauly 1asssss'),
(5, 'ngcobo@gmail.com', '0729549537', 'Gugu', 'Ngcobo', '1997-12-12', '9712120365086', 'South African', 'No', 'none', 'Female', 'National Diploma or Advanced certificate', 'Black', 'Employed', 15, 'Finance/Accounting', 'Yes', 'Queens', 'KwaZulu-Natal', 'Johannesburg', 4556, 4500, 'Monthly', 'Full-Time', 'Senior', '2-3 Weeks', 'Standard Bank', 'trocadoj', 'gugu'),
(12, 'tedy@gmail.com', '0835911601', 'Jose', 'Jenny', '1995-12-05', '9512055203021', 'Non South African', 'No', 'none', 'Female', 'Honours degree, Post Graduate diploma or Professional Qualifications', 'White', 'Unemployed', 2, 'Logistics', 'No', 'Queens', 'North West', 'CapeState', 15000, 20000, 'Weekly', 'Part-Time', 'Junior', '2-3 Weeks', 'UJ', 'aayush__14', 'aayush--14'),
(11, 'tedy@gmail.com', '0835911601', 'Jose', 'Jenny', '1995-12-05', '9512055203021', 'Non South African', 'No', 'none', 'Female', 'Honours degree, Post Graduate diploma or Professional Qualifications', 'White', 'Unemployed', 2, 'Logistics', 'No', 'Queens', 'North West', 'CapeState', 15000, 20000, 'Weekly', 'Part-Time', 'Junior', '2-3 Weeks', 'UJ', 'aayush__14', 'aayush--14'),
(9, 'tedy@gmail.com', '0835911601', 'Tedy', 'Novela', '1994-12-03', '9412035205081', 'South African', 'No', 'none', 'Male', 'National Diploma or Advanced certificate', 'Black', 'Employed', 3, 'IT/Computer', 'Yes', 'Primrose', 'Northern Cape', 'Johannesburg', 15000, 45000, 'Monthly', 'Full-Time', 'Intermediate', 'Beyond 6 Weeks', 'SARS', '', ''),
(13, 'tedy@gmail.com', '0835911601', 'Jose', 'Jenny', '1995-12-05', '9512055203021', 'Non South African', 'No', 'none', 'Female', 'Honours degree, Post Graduate diploma or Professional Qualifications', 'White', 'Unemployed', 2, 'Logistics', 'No', 'Queens', 'North West', 'CapeState', 15000, 20000, 'Weekly', 'Part-Time', 'Junior', '2-3 Weeks', 'UJ', 'aayush__14', 'aayush--14'),
(14, 'tedytinga@hotmail.com', '0740665501', 'Tedy', 'Trocado', '1997-12-11', '971252050801', 'South African', 'No', 'none', 'Male', 'Grade 9', 'Coloured', 'Employed', 13, 'IT/Computer', 'Yes', 'Primrose', 'Northern Cape', 'Kennedy', 15000, 45000, 'Monthly', 'Full-Time', 'Junior', '2-3 Weeks', 'SARS', 'trocadoj', 'aayush--14'),
(15, 'jenny@gmail.com', '0740665501', 'Jose', 'Trocado', '1997-12-11', '971252050801', 'South African', 'No', 'none', 'Male', 'Grade 9', 'Coloured', 'Employed', 1, 'IT/Computer', 'Yes', 'Queens', 'Gauteng', 'Johannesburg', 150000, 450000, 'Monthly', 'Full-Time', 'Intermediate', '2-3 Weeks', 'SARS', 'trocadoj', 'aayush--14'),
(16, 'jenny@gmail.com', '0740665501', 'Jose', 'Trocado', '1997-12-11', '971252050801', 'South African', 'No', 'none', 'Male', 'National Diploma or Advanced certificate', 'Coloured', 'Employed', 1, 'IT/Computer', 'Yes', 'Queens', 'Gauteng', 'Johannesburg', 150000, 450000, 'Monthly', 'Full-Time', 'Intermediate', '2-3 Weeks', 'SARS', 'trocadoj', 'aayush--14'),
(17, 'jenny@gmail.com', '0740665501', 'Jose', 'Trocado', '1997-12-11', '971252050801', 'South African', 'No', 'none', 'Male', 'National Diploma or Advanced certificate', 'Coloured', 'Employed', 1, 'IT/Computer', 'Yes', 'Queens', 'Gauteng', 'Johannesburg', 150000, 450000, 'Monthly', 'Full-Time', 'Intermediate', '2-3 Weeks', 'SARS', 'trocadoj', 'aayush--14'),
(18, 'miltonj@gmail.com', '0740665501', 'Milton', 'James', '1997-12-11', '9712115205081', 'South African', 'No', 'None', 'Male', 'National Diploma or Advanced certificate', 'Coloured', 'Employed', 2, 'IT/Computer', 'Yes', 'Ridgeway', 'Gauteng', 'Johannesburg', 20000, 40000, 'Monthly', 'Full-Time', 'Intermediate', 'Beyond 6 Weeks', 'FNB', 'jmilton12', 'milton@jamess'),
(19, 'aussy@gmail.com', '0740665501', 'sis', 'Diresta', '1998-05-14', '9505145016080', 'South African', 'No', 'None', 'Male', 'National Diploma or Advanced certificate', 'Black', 'Employed', 3, 'IT/Computer', 'Yes', 'Ridgeway', 'Gauteng', 'Johannesburg', 15000, 20000, 'Monthly', 'Full-Time', 'Senior', '0-1 Week', 'FNB', 'jmilton12', 'pauly 1ass'),
(20, 'aussy@gmail.com', '0740665501', 'sis', 'Diresta', '1998-05-14', '9505145016080', 'South African', 'No', 'None', 'Male', 'Masters degree', 'Black', 'Employed', 3, 'IT/Computer', 'Yes', 'Ridgeway', 'Gauteng', 'Johannesburg', 15000, 20000, 'Monthly', 'Full-Time', 'Senior', '0-1 Week', 'FNB', 'jmilton12', 'pauly 1ass');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
CREATE TABLE IF NOT EXISTS `user_login` (
  `user_email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `active` varchar(50) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `reset_complete` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_email`, `password`, `active`, `reset_token`, `reset_complete`) VALUES
('siya@gmail.com', '123123', 'Yes', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_skills`
--

DROP TABLE IF EXISTS `user_skills`;
CREATE TABLE IF NOT EXISTS `user_skills` (
  `user_skills_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL,
  `skill_name` varchar(255) NOT NULL,
  `skill_level` varchar(255) NOT NULL,
  `additionalskill` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`user_skills_id`),
  KEY `user_email` (`user_email`),
  KEY `user_skill` (`skill_level`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_skills`
--

INSERT INTO `user_skills` (`user_skills_id`, `user_email`, `skill_name`, `skill_level`, `additionalskill`) VALUES
(5, 'russ@gmail.com', 'Programming', 'Expert', 'Expert in Java, C sharp, and C++. Also managing databases'),
(6, 'tedy@gmail.com', 'Information Technology', 'Intermediate', 'C#, Java, C+++                          \r\n                                    '),
(7, 'tedy@gmail.com', 'Information Technology', 'Intermediate', 'C#, Java , Python, C++      '),
(8, 'tedy@gmail.com', 'Information Technology', 'Intermediate', 'C#, Java, Python                             \r\n                                    '),
(9, 'miltonj@gmail.com', 'Information Technology', 'Intermediate', 'C# , Java , Python                     \r\n                                    '),
(10, 'aussy@gmail.com', 'Engineering', 'Expert', 'dfhgfdhgfdhg');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

DROP TABLE IF EXISTS `vacancy`;
CREATE TABLE IF NOT EXISTS `vacancy` (
  `vacancy_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_name` varchar(30) NOT NULL,
  `vacancy_description` varchar(255) NOT NULL,
  `required_education` varchar(255) NOT NULL,
  `required_skill` varchar(255) DEFAULT NULL,
  `required_skill_level` varchar(255) NOT NULL,
  `years_of_experience` int(10) NOT NULL,
  `salary` int(50) NOT NULL,
  `post_date` datetime DEFAULT NULL,
  `kill_education` varchar(5) NOT NULL,
  `kill_experience` varchar(5) NOT NULL,
  `kill_disability` varchar(5) NOT NULL,
  `confirmed` varchar(50) NOT NULL DEFAULT 'no',
  `reasonDeclined` varchar(255) DEFAULT NULL,
  `required_employment_level` varchar(50) NOT NULL,
  PRIMARY KEY (`vacancy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`vacancy_id`, `vacancy_name`, `vacancy_description`, `required_education`, `required_skill`, `required_skill_level`, `years_of_experience`, `salary`, `post_date`, `kill_education`, `kill_experience`, `kill_disability`, `confirmed`, `reasonDeclined`, `required_employment_level`) VALUES
(1, 'Database Admin', 'Someone to manage our Database Systems', 'Degree', NULL, '', 2, 20000, '2019-09-12 11:53:38', 'No', 'Yes', 'Yes', 'confirmed', '', ''),
(2, 'Tester', 'Testing the code for us\r\n\r\n\r\n\r\n', 'Degree', NULL, '', 0, 5000, '2019-10-01 08:56:32', '0', '0', '0', 'confirmed', 'missing education level', ''),
(3, 'hr', 'hr management', '', NULL, '', 5, 10000, '2019-10-18 12:51:56', 'Yes', 'Yes', 'Yes', 'denied', 'Not needed. position already filled', ''),
(4, 'senior programmer', 'must know c#, java, sql, mysql, python', '', NULL, '', 5, 30000, '2019-10-18 12:09:53', '0', '0', '0', 'no', 'missing education level', ''),
(5, 'database administrator ', 'maintain the integrity of the databases and enforce database security', '', NULL, '', 2, 30000, '2019-10-18 12:35:05', '0', '0', '0', 'no', 'salary should be 25000', ''),
(6, 'Data Capturing', 'You will be required to capture data that is given to you into databases.', 'Grade Twelve (Standard Ten / Matric)', NULL, '', 0, 3000, '2019-09-30 21:16:56', '0', '0', '0', 'confirmed', NULL, ''),
(7, 'accountant', 'work with our money', 'Honours Degree', NULL, '', 2, 26000, '2019-10-01 09:35:50', 'Yes', 'Yes', 'No', 'confirmed', NULL, ''),
(8, 'business marketer', 'be in charge of business marketing', '', NULL, '', 2, 25000, '2019-10-26 22:32:05', 'No', 'No', 'No', 'confirmed', '', ''),
(9, 'SQL Developer', 'Manage Database', '', 'Information Technology', 'Amateur', 2, 20000, '2019-10-21 17:56:20', '0', '0', '0', 'confirmed', '', ''),
(10, 'SQL Developer', 'sql programming skills', '', 'Information Technology', 'Advanced', 2, 20000, '2019-10-27 00:16:49', '0', '0', '0', 'denied', '', ''),
(11, 'sales manager', 'manages the sales of the business and oversees accounts.', 'Bachelors degree, Advanced Diplomas, Post Graduate Certificate or Btech', 'Bookkeeping', 'Advanced', 3, 25000, '2019-10-28 01:06:53', 'Yes', 'No', 'Yes', 'confirmed', NULL, 'Senior');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_skill_requirement`
--

DROP TABLE IF EXISTS `vacancy_skill_requirement`;
CREATE TABLE IF NOT EXISTS `vacancy_skill_requirement` (
  `vacancy_skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `vacancy_skill` varchar(255) NOT NULL,
  `skill_level` varchar(255) NOT NULL,
  `vacancy_id` varchar(255) NOT NULL,
  PRIMARY KEY (`vacancy_skill_id`),
  KEY `skill_level` (`skill_level`),
  KEY `vacancy_id` (`vacancy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy_skill_requirement`
--

INSERT INTO `vacancy_skill_requirement` (`vacancy_skill_id`, `vacancy_skill`, `skill_level`, `vacancy_id`) VALUES
(4, 'Programming', 'Expert', '4'),
(5, 'Programming', 'Expert', '1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
