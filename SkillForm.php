<!DOCTYPE html>
<html>
<head>
    <title> Skills Form</title>
    <link rel="stylesheet" type="text/css" href="cvform.css">
    <link rel="stylesheet" href="file1.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php
session_start();
$conn = new mysqli("localhost", "root", "", "hr_system");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$email = $_GET['email'];
?>

</head>
<body>


<h4 class= "text-center">We on the Last Step...</h4>
<h6 class= "text-center">Complete your details below:</h6>
<h6 class= "text-center">IF YOU WISH TO SUBMIT YOUR APPLICATION, PLEASE CLICK ON SUBMIT</h6>

<form method="POST" action="skill.php" accept-charset="UTF-8"  name="CV form" id="">
    <hr>
    <div class="formBlock" style="border-radius: 20px; padding: 20px; background: silver; height: auto; width: 800px; margin-left: 25%;">
                               <h2 align="left">Enter your Skills...</h2>                               
                               <small>* This page is very important for us.</small>
                               <small>Enter your information accurately</small>

                               <hr style="height:30px" color="blue">
                               <br>

                            <div class="row">
                             <div class="col-lg-6"> 
                                 <div class= column  id = "emailContainer">
                                    <div class="formLabel">
                                        <label>Email <span class="mustFillin">* </span>
                                            </label>
                                    </div>
                    
                                    <div class="FormInput">
                                        <input value="<?php echo $email ?>"  id="reg-email"  type="email" name="email" readonly required>
                                    </div>
                    
                                </div>
                            </div>
                        </div>

                        <hr>
                        <br>

                               <div class="row">
                                <div class="col-lg-6">
                                        <div  id="skills">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Skills :
                                            </label>
                                        </div>
                                    
                                        </div>
                                        <div class="formInput">
                                            
                                            <select id="skill" name="skill" required ><option> -- Name -- </option><option>Accounting</option><option>Administrative</option><option>Analysis</option><option>Analytics</option><option>Automotive</option><option>Banking</option><option>Bookkeeping</option><option>Carpentry</option><option>Computer</option><option>Construction</option><option>Data</option><option>Design</option><option>Editing</option><option>Electrical</option><option>Engineering</option><option>Financial</option><option>Hardware</option><option>Healthcare</option><option>Information Technology</option><option>Languages</option><option>Legal</option><option>Manufacturing</option><option>Math</option><option>Mechanical</option><option>Medical</option><option>Nursing</option><option>Optimization</option><option>Pharmaceutical</option><option>Pipefitter</option><option>Plumbing</option><option>Project Management</option><option>Programming</option><option>Research</option><option>Reporting</option><option>Science</option><option>Software</option><option>Spreadsheets</option><option>Teaching</option><option>Technology</option><option>Testing</option><option>Translation</option><option>Transcription</option><option>Word Processing</option><option>Writing</option></select> 
                                            <br>
                                        </div>
                                            <div class="formInput">
                                                
                                            <select id="skilllevel" name="skilllevel" required><option> -- Level -- </option><option>Beginner</option><option>Amateur</option><option>Intermediate</option><option>Advanced</option><option>Expert</option></select> 
                                            <br>
                                           
                                                                                  
                                                                                      
                                            </div>
                                </div>
                                <div class="col-lg-6"> 
                                    <div class="formLabel">
                                        <label>Additional Skills <br>
                                            <h6>e.g [ C#, Analyst, Bookkeping ]</h6>
                                            </label>
                                    </div>
                                 <textarea rows="4" cols="50" name="additionalskill" id="additionalskill">                             
                                    </textarea>
                            </div>
                            <form>
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-info" name="addbtn" id="addbtn" style="border: 1px solid"> Add skill </button>
                                
                               
                                </div>
                                <hr style="height:30px" color="blue">
<hr>
<br>
         <div><a href="thankyou.php" class="btn btn-info"><u>Submit</u></a></div>
     </form>
        
<hr>
</form>



</body>
</html> 
