<!DOCTYPE html>
<html>
<head>
	<title>Admin Page</title>
  <link rel="stylesheet" type="text/css" href="admin.css">
  <link rel="stylesheet" type="text/css" href="admin_style.css">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/d7e01028ae.js" crossorigin="anonymous"></script>
<style>
  a:link {
   color: black;
}

a:visited {
    color: black;
}

a:hover {
    color: blue;

}
</style>
</head>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
<div class="dropdown">
    <i class="fas fa-bars" style = "color:white;" class = "dropdown-toggle" data-toggle = "dropdown"></i>
    <div class="dropdown-menu">
	<a class="dropdown-item" href="hr_home.php">Back</a>
    <a class="dropdown-item" href="admin1.php?logout='1'">Logout</a>
    </div>
    </div>
    <label class = "site-heading">OPG-HR</label>
</nav>
<body onload="RankApplicants();">
	<script src="admin.js"></script>
  <div class="text-center">
	<h3>Top Applicants:</h3>
</div>

<div id = "loader"></div>
<div class="jumbotron">
<h2><i class="fas fa-scroll fa-2x" style = "padding: 10px;"></i> Applicant Rankings For: <?php  session_start();
    $conn = new mysqli("localhost","root","","hr_system");
    if(!$conn){
      echo "connection failed".mysqli_connect_error();
    } 
    $id = $_GET['id'];

    $vac_name = "SELECT vacancy_name FROM vacancy WHERE vacancy_id = $id";
    $result1 = mysqli_query($conn, $vac_name);

    while( $row = $result1->fetch_assoc()){
      echo $row["vacancy_name"];
    }
    ?></h2>
<hr>
  </div>
<form id = "rankingList" style = "display: none">

    <?php
   
    $sql1 = 'SELECT ranking.vacancy_id, ranking.points, ranking.email, vacancy.vacancy_name FROM ranking INNER JOIN vacancy ON ranking.vacancy_id = vacancy.vacancy_id WHERE kill_decision = "No" AND vacancy.vacancy_id = '.$id.' ORDER BY points DESC;';
    $result = mysqli_query($conn, $sql1);

    if($result)
    {
      $count = 1;
      if($result ->num_rows > 0){
        while($row = $result->fetch_assoc()){
          $vac_ID = $row["vacancy_id"];
          $points_ = $row["points"];
          $email = $row["email"];
          $vacancy = $row["vacancy_name"];
  
          $parameterAccept = "showEmailAccept('".$email."')";
          $parameterDecline = "showEmailDecline('".$email."')";
  
          echo '
        
          <div class="card bg-light text-dark" style ="width: 100%>
             <div class="card-body">
              <h4 class="card-title"><a  href=pdf.php?email='.$email.'>'.$email.'</a></h4>
              <br>
              <h6> Position: '.$count.'</h6>
              <p class="card-text">Vacancy ID: '.$vac_ID.'    |  Points: '.$points_.'</p>
              <div class="dropdown">
  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
    Options
  </button>
  <div class="dropdown-menu">
  <a  id = "btn-accept" class="dropdown-item" onclick = "'.$parameterAccept.'" value = "'.$email.'" data-toggle="modal" data-target="#myModal">Accept</a>
  <a id = "btn-decline" class="dropdown-item" onclick = "'.$parameterDecline.'" value = "'.$email.'" data-toggle="modal" data-target="#myModal">Decline</a>
  <a class = "dropdown-item" href=view.php?email='.$email.'>View Documents</a>
  </div>
</div>
             </div>
         </div>
       
         <br>
          ';

          $count +=1;
          ;
  
        }
        

    }
    else
    {
      echo '<div class="card bg-info text-white">
      <div class="card-body"><h3>There are no applicants for this job.<i class="far fa-sad-cry"></i></h3></div>
    </div>';
    }

    
    }
    else
    {
      echo $id;
    }

    
    ?>
  <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h6>To: <label id = "email_recipient"></label></h6>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <h6 style = "text-align: left">Subject: </h6>
        <input type="text" style= "float: left; min-width: 400px" id = "email_subject">
        <br>
        <br>
        <h6 style = "text-align: left">Body: </h6>
       <textarea name="email_content" id="email_content" cols="60" rows="10" style = "float: left"></textarea>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" onclick = "sendEmail()">Send</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>

    </div>
  </div>
</div>
</form>
<script type="text/javascript">

function RankApplicants(){
  var var1 = setTimeout(callRankingService,2000);
  var var_ = setTimeout(showRankings,3000);
}

function showRankings(){

  document.getElementById("loader").style.display = "none";
  document.getElementById("rankingList").style.display = "block";

}

function callRankingService(){
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "http://localhost:8090/rank/applicants/ranking", true);
  xhttp.send();
  xhttp.close();
}

function showEmailAccept(email_){
  document.getElementById('email_recipient').innerHTML = email_;
  document.getElementById('email_subject').value = "OPG-HR - Your Job Application";
  document.getElementById('email_content').value = "Greetings, \n\n We have received and looked through your CV and have decided to call you for an interview on the following date and time: \n\n Kind Regards, \n OPG HR-Manager";
  

}

function showEmailDecline(email_){
  document.getElementById('email_recipient').innerHTML = email_;
  document.getElementById('email_subject').value = "OPG-HR - Your Job Application";
  document.getElementById('email_content').value = "Greetings, \n\n This email serves to inform you that unfortunately you have not been considered for the Job opening. THank you for applying. \n\n Regards, \n OPG HR-Manager";

}

function sendEmail(){
   var recipient = document.getElementById('email_recipient');
   var subject = document.getElementById('email_subject').value;
   var content = document.getElementById('email_content').value;
   var xhttp = new XMLHttpRequest();
  xhttp.open("POST","http://localhost:8085/email/send/"+recipient+"/with/"+subject+"/and/" + content,true);
  xhttp.send();
  xhttp.close();
  window.confirm("Email is sent.");
}

</script>
</body>
</html>