<!DOCTYPE html>
<html>
<head>
    <title>Vacancy Details</title>
    <!-- <link rel="stylesheet" type="text/css" href="addvacancy.css"> -->
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/d7e01028ae.js" crossorigin="anonymous"></script>

</head>
<body  >
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <div class="dropdown">
    <i class="fas fa-bars" style = "color: white;" class = "dropdown-toggle" data-toggle = "dropdown"></i>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="admin1.php">Home</a>
    <a class="dropdown-item" href="admin1.php?logout='1'">Logout</a>
  </div>
    </div>
    <label class = "site-heading" style = "color: white; padding-left: 100px;">OPG-Admin</label>
</nav>
<div class="jumbotron">
  <h3> View/Edit details for following Vacancy:</h3>
  <hr>
<?php

session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hr_system";

  $vacancy_name;
  $vacancy_description;
  $required_education;
  $years_expirience;
  $salary ;
  $id = $_GET['id'];

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} ;

$sql = "SELECT * FROM `vacancy` WHERE `vacancy_id` =$id;";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    // output data of each row
    if($row = $result->fetch_assoc()) {

      
        echo  "<h5> <a href='ash.php?id=$id'>" .$row["vacancy_name"]. "</a></h5><br><b>  Description:</b> ". $row["vacancy_description"]. "<br><b>  Minimum Education:</b> ". $row["required_education"]."<b><br> Salary: </b> ". $row["salary"]. "<b><br> Posted: </b> ". $row["post_date"]."</b><br><b><br> Reason why declined: </b> ". $row["reasonDeclined"]."</b><br>";
          
  $vacancy_name = $row["vacancy_name"];
  $vacancy_description=$row["vacancy_description"];
  $required_education=$row["required_education"];
  $years_of_experience=$row["years_of_experience"];
  $salary=$row["salary"];
  $required_skill = $row["required_skill"];
  $required_skill_level = $row["required_skill_level"];
  $kill_education = $row["kill_education"];
  $kill_experience = $row["kill_experience"];
  $kill_disability = $row["kill_disability"];

  }
     else 
    {
       echo "error has accured";
     
    }
}
?>
</div>

<form class="sform" method="POST" action="update.php?id=<?php echo $id;?>">
<div class="card bg-light">
<div class="card-body" style = "width: 800px;">
        <div class="row">
            <div class="col-lg-6"><h6>Job Title*</h6><input type="text" name="vacancy_name" value = "<?php echo $vacancy_name ?>"></div>
            <div class="col-lg-6">
            <h6>Job Description*</h6><textarea name="vacancy_description" style="width:100%; height:100px;"><?php
            echo $vacancy_description;
            ?></textarea>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-12">
                <h6>Minimum Education*</h6>
            <div class="formInput">
                                            <select  name="educationLevel"><option value="0"> <?php echo $required_education;?></option><option>Doctor's degree</option><option>Master's degree</option><option>Honours degree, Post Graduate diploma or Professional Qualifications</option><option>Bachelor's degree, Advanced Diplomas, Post Graduate Certificate and B-tech</option><option>National Diploma or Advanced certificate</option><option>Higher Certificates and Advanced National (vocational) Cert.</option><option>Grade 12 (National Senior Certificate) or National (vocational) Cert. level 4</option><option>Grade 11 or National (vocational) Certificates level 3</option><option>Grade 10 or National (vocational) Certificates level 2</option><option>Grade 9</option></select><small id="educationLevelValidation"></small>

                                        </div>
            </div>
    </div>
    <br>
    <div class="row">
    <div class="col-lg-12">
                <h6>Required Skills*</h6>
            <div class="formInput">
                                <select id="skill" name="skill" required ><option value="0"><?php echo $required_skill ;?></option><option>Accounting</option><option>Administrative</option><option>Analysis</option><option>Analytics</option><option>Automotive</option><option>Banking</option><option>Bookkeeping</option><option>Carpentry</option><option>Computer</option><option>Construction</option><option>Data</option><option>Design</option><option>Editing</option><option>Electrical</option><option>Engineering</option><option>Financial</option><option>Hardware</option><option>Healthcare</option><option>Information Technology</option><option>Languages</option><option>Legal</option><option>Manufacturing</option><option>Math</option><option>Mechanical</option><option>Medical</option><option>Nursing</option><option>Optimization</option><option>Pharmaceutical</option><option>Pipefitter</option><option>Plumbing</option><option>Project Management</option><option>Programming</option><option>Research</option><option>Reporting</option><option>Science</option><option>Software</option><option>Spreadsheets</option><option>Teaching</option><option>Technology</option><option>Testing</option><option>Translation</option><option>Transcription</option><option>Word Processing</option><option>Writing</option></select> 
                                        </div>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Skill Level*</h6>
            <div class="formInput">
                                <select id="skilllevel" name="skilllevel"><option> <?php  echo $required_skill_level ;?> </option><option>Beginner</option><option>Amateur</option><option>Intermediate</option><option>Advanced</option><option>Expert</option></select> 
                                        </div>
            </div>
            <div class="col-lg-6">
            <h6>Years of experience*</h6>
            <input type="number" name="years_of_experience" value = "<?php echo $years_of_experience ; ?>">
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Salary*</h6>
            <input type="number" name="salary" value = "<?php echo $salary ;?>">
            </div>
            
            <div class="col-lg-6">
            <h6>Kill Underqualified?*</h6>
                 <div class="formInput">
                                            <select  name="killEducation"><option value="0"> <?php echo $kill_education ;?> </option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div></div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Kill Under-experienced?*</h6>
            <div class="formInput">
                                            <select  name="killExperience"><option value="0"> <?php echo $kill_experience ;?></option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div>
            </div>
            <div class="col-lg-6">
<h6>Kill Disability?*</h6>
                <div class="formInput">
                                            <select  name="killDisability"><option value="0"> <?php  echo $kill_disability ;?></option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div></div>
    </div>
   </div>
</div>
<div class="card-footer">
<input type="submit"  value="Update Vacancy" class = "btn btn-info">
    
<form style="margin: -64px auto 0px; position: absolute; right: 50%" method="POST" action="deleteConfirm.php?id=<?php echo $id;?>">
     <input type="submit" value="Delete this vacancy" class = "btn btn-danger">
</form>
</div>
    
</div>
</form>

<br><br>




