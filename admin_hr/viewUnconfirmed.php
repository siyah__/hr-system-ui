<?php 
include('functions.php');
//include('admin.js');

if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="admin1.css">
	<style>
	.header {
		 background: linear-gradient(75deg, #2980b9, #2c3e50) top left fixed;
	}
	button[name=register_btn] {
		background: #003366;
	}
	</style>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/d7e01028ae.js" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <div class="dropdown">
    <i class="fas fa-bars" style = "color: white;" class = "dropdown-toggle" data-toggle = "dropdown"></i>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="admin1.php?logout='1'">Logout</a>
	<a class="dropdown-item" href="hr_home.php">back</a>
  </div>
    </div>
    <label class = "site-heading" style = "color: white; padding-left: 100px;">Human Resources - View Unconfirmed</label>
</nav>
</head>
<body>

	<div class="content">
		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
				<h3>
					<?php 
						echo $_SESSION['success']; 
						unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php endif ?>

		<!-- logged in user information -->
		<!-- div class="profile_info"> 
			<img src="C:/wamp64/www/loginregister-master/admin_hr/admin_profile.png"  >
 -->
			<div>
				<?php  if (isset($_SESSION['user'])) : ?>
				<div class="jumbotron">

					<i class="far fa-user-circle fa-3x" style = "padding: 20px"></i><?php echo $_SESSION['user']['username']; ?>
					<i  style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i>
					</div>

				<?php endif ?>
			</div>
		</div>
	</div>
	<h3><b>Unconfirmed Vacancies Currently Listed</b></h3>
	<br>
	<hr>

<?php 
     include 'jobUnconfirmed.php';
   ?>
	<!-- <div id = "loader"></div>
	<div style = "display: none" id = "myDiv" class= "animate-bottom">
   
</div> -->
	
</body>
<script>
var myVar;

function myFunction(){
    myVar = setTimeout(showPage, 3000);

}

function showPage(){
    
    document.getElementById("loader").style.display = "none";
    document.getElementById("myDiv").style.display = "block";

}
</script>
</html>