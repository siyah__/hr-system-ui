<!DOCTYPE html>
<html>
<head>
    <title>Vacancy Details</title>
    <link rel="stylesheet" type="text/css" href="cvform.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/d7e01028ae.js" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <div class="dropdown">
    <i class="fas fa-bars" style = "color: white;" class = "dropdown-toggle" data-toggle = "dropdown"></i>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="viewUnconfirmed.php">Back</a>
    <a class="dropdown-item" href="hr_home.php">Home</a>
    <a class="dropdown-item" href="admin1.php?logout='1'">Logout</a>
  </div>
    </div>
    <label class = "site-heading" style = "color: white; padding-left: 100px;">OPG-HR</label>
</nav>
<div class ="jumbotron">
<h3> Vacancy authorization:</h3>
<hr>
<?php

session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hr_system";

  $vacancy_name;
  $vacancy_description;
  $required_education;
  $years_expirience;
  $salary ;
  $id = $_GET['id'];

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM `vacancy` WHERE `vacancy_id` =$id;";
$result = $conn->query($sql);


if ($result->num_rows > 0) {
    // output data of each row
    if($row = $result->fetch_assoc()) {

      echo  "<b><h3>" .$row["vacancy_name"]. "</b></h3><br><b>  Description:</b> ". $row["vacancy_description"]. "<br><b>  Minimum Education:</b> ". $row["required_education"]. " <b><br> Salary: </b> ". $row["salary"]. "<b><br> Posted: </b> ". $row["post_date"]."<br><br>";

  $vacancy_name = $row["vacancy_name"];
  $vacancy_description=$row["vacancy_description"];
  $required_education=$row["required_education"];
  $years_of_experience=$row["years_of_experience"];
  $salary=$row["salary"] ;
  }
     else 
    {
       echo "error has accured";
     
    }
}
?>

<div class="card bg-light">
<form method="POST" action="hr_confirm_deny.php?id=<?php echo $id;?>">
<div class="card-body" style = "width: 800px;">
        <div class="row">
        <div class="col-lg-6">
     <label for="right-label"><h6>Add reason (if denied)</h6><span class="mustFillin">*</span>
                                </label><textarea name="vacancy_description" style="width:900px; height:100px;">
</textarea><br>
</div>
</div>
</div>

<br>   
<div class ="card-footer"> 
<div class="btn-group">

   <input type="submit"  value="Decline this Vacancy"  class = "btn btn-danger">

</form>
<form method="POST" action="hr_confirm_update.php?id=<?php echo $id;?>">
     <input type="submit" value="Confirm this vacancy"  class = "btn btn-info">
</form>
</div>
</div>




</div>
<br><br>




