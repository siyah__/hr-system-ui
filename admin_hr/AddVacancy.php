<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>addvacancy</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/d7e01028ae.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="addvacancy.css">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <div class="dropdown">
    <i class="fas fa-bars" style = "color: white;" class = "dropdown-toggle" data-toggle = "dropdown"></i>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="admin1.php">Home</a>
    <a class="dropdown-item" href="admin1.php?logout='1'">Logout</a>
  </div>
    </div>
    <label class = "site-heading" style = "color: white; padding-left: 100px;">OPG-HR</label>
</nav>
<div class="header">
<h2 > Add New Vacancy <br></h2>
</div>
<!-- <input type="Button" onclick="window.location.href='admin1.php'" value="back" style="padding: 10px;
    font-size: 11px;
    color: white;
    background: grey;
    border: none;
    border-radius: 5px;
    position: absolute;
    right:0%"> <br><br> -->
 
<form method="POST" action="Vacancy_Process.php" class="sform">

<div class="card bg-light text-dark" style = "min-width: 800px; margin-left: 5%; margin-top: 5%; padding: 15px;">
    <div class="card-body">
        <div class="card-header">
            <h3>Create a vacancy</h3>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6"><h6>Job Title*</h6><input type="text" name="vacancy_name"></div>
            <div class="col-lg-6">
            <h6>Job Description*</h6><textarea name="vacancy_description" style="width:100%; height:100px;"></textarea>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Minimum Education*</h6>
            <div class="formInput">
            <select  name="educationLevel"><option value="0"> -- select -- </option><option>Doctors degree</option><option>Masters degree</option><option>Honours degree, Post Graduate diploma or Professional Qualifications</option><option>Bachelors degree, Advanced Diplomas, Post Graduate Certificate or Btech</option><option>National Diploma or Advanced certificate</option><option>Higher Certificates and Advanced National (vocational) Cert.</option><option>Grade 12 (National Senior Certificate) or National (vocational) Cert. level 4</option><option>Grade 11 or National (vocational) Certificates level 3</option><option>Grade 10 or National (vocational) Certificates level 2</option><option>Grade 9</option></select><small id="educationLevelValidation"></small>
                                        </div>
            </div>
            <div class="col-lg-6">
            <h6>Required Emplyment Level*</h6>
            <select id="employment_level" name="employment_level"><option value="0"> -- select -- </option><option>Graduate</option><option>Junior</option><option>Intermediate</option><option>Senior</option></select> 

            </div>
    </div>
    <br>
    <div class="row">
    <div class="col-lg-12">
                <h6>Required Skills*</h6>
            <div class="formInput">
                                <select id="skill" name="skill" required ><option value="0"> -- Name -- </option><option>Accounting</option><option>Administrative</option><option>Analysis</option><option>Analytics</option><option>Automotive</option><option>Banking</option><option>Bookkeeping</option><option>Carpentry</option><option>Computer</option><option>Construction</option><option>Data</option><option>Design</option><option>Editing</option><option>Electrical</option><option>Engineering</option><option>Financial</option><option>Hardware</option><option>Healthcare</option><option>Information Technology</option><option>Languages</option><option>Legal</option><option>Manufacturing</option><option>Math</option><option>Mechanical</option><option>Medical</option><option>Nursing</option><option>Optimization</option><option>Pharmaceutical</option><option>Pipefitter</option><option>Plumbing</option><option>Project Management</option><option>Programming</option><option>Research</option><option>Reporting</option><option>Science</option><option>Software</option><option>Spreadsheets</option><option>Teaching</option><option>Technology</option><option>Testing</option><option>Translation</option><option>Transcription</option><option>Word Processing</option><option>Writing</option></select> 
                                        </div>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Skill Level*</h6>
            <div class="formInput">
                                <select id="skilllevel" name="skilllevel" required><option value="0"> -- Level -- </option><option>Beginner</option><option>Amateur</option><option>Intermediate</option><option>Advanced</option><option>Expert</option></select> 
                                        </div>
            </div>
            <div class="col-lg-6">
            <h6>Years of experience*</h6>
            <input type="number" name="years_of_experience">
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Salary*</h6>
            <input type="number" name="salary">
            </div>
            
            <div class="col-lg-6">
            <h6>Kill Underqualified?*</h6>
                 <div class="formInput">
                                            <select  name="killEducation"><option value="0"> -- select -- </option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div></div>
    </div>
    <br>
    <div class="row">
            <div class="col-lg-6">
                <h6>Kill Under-experienced?*</h6>
            <div class="formInput">
                                            <select  name="killExperience"><option value="0"> -- select -- </option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div>
            </div>
            <div class="col-lg-6">
<h6>Kill Disability?*</h6>
                <div class="formInput">
                                            <select  name="killDisability"><option value="0"> -- select -- </option><option>Yes</option><option>No</option></select><small id="educationLevelValidation"></small>
                                        </div></div>
    </div>
   </div>
   <div class="card-footer">
<input type="submit"   value="Add New Vacancy" class="btn btn-primary">
   </div>
</div>
        
</form> 
    
</body>
</html>

   