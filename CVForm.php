<!DOCTYPE html>
<html>
<head>
    <title> CV FORM</title>
    <link rel="stylesheet" type="text/css" href="cvform.css">
    <link rel="stylesheet" href="file1.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


</head>
<body>

<h3 class= "text-center"> Complete your application for: <br></h3>
<?php

session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hr_system";

$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$id = $_GET['id'];
$sql = "SELECT * FROM vacancy WHERE vacancy_id=$id";
$result = $conn->query($sql);



if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      
      echo  "<b><h3>" .$row["vacancy_name"]. "</b></h3><br><b>  Description:</b> ". $row["vacancy_description"]. "<b><br> Salary: </b> ". $row["salary"]. "<b><br> Posted On: </b> ". $row["post_date"]."<br><br>";
    }
} else {   
    echo "0 results";
     
  }  
if (isset($_POST['submit']))
{
   $email= $_POST['email'];

  $query = ("INSERT INTO application ([user_email],[vacancy_id]) VALUES ('$email',$id) ");
  if (mysql_query($query))
 {
echo "<script>alert('INSERTED SUCCESSFULLY');</script>";
}
else
 {
 echo "<script>alert('FAILED TO INSERT');</script>";
 }

 }







$sql1 = "SELECT * FROM `user_details` WHERE `email` = `members.email`";
$result1 = $conn->query($sql1);



?>


<hr>

<form method="POST" action="cv_process.php?id=<?php echo $id;?>" accept-charset="UTF-8"  name="CV form" id="">



<div>

    <div></div>

    <div class="formBlock" style="border-radius: 20px; padding: 20px; background: silver; height: auto; width: 800px; margin-left: 25%;">
         
         <div class="row">
             <div class="col-lg-6"> 
                 <div class= column  id = "emailContainer">
                    <div class="formLabel">
                        <label>Email <span class="mustFillin">* </span>
                            </label>
                    </div>
    
                    <div class="FormInput">
                        <input placeholder="E-mail"  id="reg-email"  type="email" name="email" required>
                    </div>
    
                </div>
            </div>
             <div class="col-lg-6">
                    <div  id="mobileContainer">
                            <div class="formLabel">
                                <label for="right-label">Contact Number <span class="mustFillin">*</span>
                                </label>
                            </div>
                            <div class="formInput ">
                                <input id="mobile" placeholder="e.g. 0833333333"  name="contact_number" type="text" required>      
                            </div>
                        </div>

             </div>
         </div>
         <hr>
         <br>

         <div class="row">
             <div class="col-lg-6">
                    <div id="nameContainer">
                            <div class="formLabel">
                                <label>Name <span class="mustFillin"> * </span></label>
                            </div>
           
                            <div class="formInput">
                                <input type="text" placeholder="Name" name="name" required>
                            </div>
                        </div>
             </div>
             <div class="col-lg-6">
                    <div id="surnameContainer">
                            <div class="formLabel">
                                <label for="right-label">
                                    Surname <span class="mustFillin">*</span>
                                </label>
                            </div>

                            <div class="formInput">
                                <input id="surname" placeholder="Surname"  name="surname" type="text" required>                                                                <small id="surnameValidation"></small>
                            </div>
                        </div>
             </div>
         </div>
         <hr>
         <br>
         <div class="row">
             <div class="col-lg-6">

                    <div id="nameContainer">
                            <div class="formLabel">
                                <label>Date-of-birth <span class="mustFillin"> * </span></label>
                            </div>
           
                            <div class="formInput">
                                <input type="date" placeholder="dd/mm/yyyy" name="dob" required>
                            </div>
                        </div>
             </div>
             <div class="col-lg-6">
                    <div  id="passwordContainer">
                            <div class="formLabel">
                                <label for="right-label">
                                    South African ID number <br/>
                                    or Forgein Passport number <span class="mustFillin">*</span>
                                </label>
                            </div>

                            <div class="formInput">
                                <input type="number" name="id_number" id="id_number" placeholder="e.g. 9902155623025" required>
                        </div>
                    </div>
                </div>
         </div>
         <hr>
         <br>
                    <div class="row">
                        <div class="col-lg-6">
                                <div  id="citizenshipContainer">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Citizenship <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="citizenship" name="citizenship"><option value="0"> -- select -- </option><option>South African</option><option>Non South African</option></select> <small id="citizenshipValidation"></small>
                                        </div>
                                    </div>

                        </div>
                            <div class="col-lg-6">
                                <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Gender <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                    <input type="radio" name="gender" value="Male"> Male<br>
                                    <input type="radio" name="gender" value="Female"> Female<br>
                                    <small id="genderValidation"></small>
                            </div>
                    </div>
                    <hr>
                        <br>
                    <div class="row">
                        <div class="col-lg-6">
                                <div  id="educationLevelContainer">
                                        <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Highest Education Level <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput" style = "width: 200px;">
                                            <select  name="educationLevel"><option value="0"> -- select -- </option><option>Doctors degree</option><option>Masters degree</option><option>Honours degree, Post Graduate diploma or Professional Qualifications</option><option>Bachelors degree, Advanced Diplomas, Post Graduate Certificate or Btech</option><option>National Diploma or Advanced certificate</option><option>Higher Certificates and Advanced National (vocational) Cert.</option><option>Grade 12 (National Senior Certificate) or National (vocational) Cert. level 4</option><option>Grade 11 or National (vocational) Certificates level 3</option><option>Grade 10 or National (vocational) Certificates level 2</option><option>Grade 9</option></select><small id="educationLevelValidation"></small>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                        
                                <div  id="ethnicityContainer">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Ethnicity <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="ethnicity" name="ethnicity"><option value="0"> -- select -- </option><option>Asian</option><option>Black</option><option>Coloured</option><option>Indian</option><option>White</option><option>Other</option></select>                                                                <small id="ethnicityValidation"></small>
                                        </div>
                                    </div>
                        </div>
                    </div>
                    <hr>
                    <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div  id="jobTitle">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Job Title <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="job_title" name="job_title"><option value="0"> -- select -- </option><option>Employed</option><option>Unemployed</option><option>Student</option>></select> 
                                            <small id="jobTitleValidation"></small>
                                        </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div  id="industry">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Industry <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="industry" name="industry"><option value="0"> -- select -- </option><option>Administration/PA/Secretary</option><option>Automotive</option><option>Building/Construction/Mining</option><option>Clearing and Forwarding</option><option>Engineering</option><option>FMCG</option><option>Finance/Accounting</option><option>General Employment</option><option>Health and Skincare</option><option>IT/Computer</option><option>Insurance</option><option>International</option><option>Legal</option><option>Logistics</option><option>Management</option><option>Medical</option><option>Part Time/Temporary</option><option>Professions</option><option>Property/Facilities Management</option><option>Retail</option><option>Sales/Marketing</option><option>Tourism/Hospitality</option><option>Trade/Artisans/Technical</option><option>Training</option></select> 
                                            <small id="industryValidation"></small>
                                        </div>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                                <div class="col-lg-6">
                                        <div  id="relocate">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Willing to Relocate? <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="relocate" name="relocate"><option value="0"> -- select -- </option><option>Yes</option><option>No</option></select> 
                                            <small id="relocateValidation"></small>
                                        </div>
                                </div>
                                </div>
                                <div class="col-lg-6">
                                        <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Suburb <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                        <input id = "region" name="region" type="text" placeholder="Suburb">
                                </div>
                            </div>

						<hr>
                        <br>
                        
                            <div class="row">
                                    <div class="col-lg-6">
                                            <div  id="province">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Province <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="province" name="province"><option value="0"> -- select -- </option><option>Limpopo</option><option>Eastern Cape</option><option>Free State</option><option>Northern Cape</option><option>Gauteng</option><option>KwaZulu-Natal</option><option>Mpumalanga</option><option>Western Cape</option><option>North West</option></select> 
                                            <small id="provinceValidation"></small>
                                        </div>
                                    </div>
        
                                    </div>
                                    <div class="col-lg-6">
                                            <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                City <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                            <input id = "city" name="city" type="text" placeholder="City">
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <div class="row">
                                        <div class="col-lg-6">
                                            <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Minimum Salary Expected <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                            <input id = "min_salary" name="min_salary" type="number" placeholder="Enter Amount">
            
                                        </div>
                                        <div class="col-lg-6">
                                                <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Maximum Salary Expected <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                                <input id = "max_salary" name="max_salary" type="number" placeholder="Enter Amount">
                                            
                                        </div>
                                    </div>
                                    <hr>
                                    <br>
                                    <div class="row">
                                            <div class="col-lg-6">
                                                    <div  id="pty">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Payment Type <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="payment_type" name="payment_type"><option value="0"> -- select -- </option><option>Weekly</option><option>Monthly</option><option>Yearly</option></select> 
                                            <small id="paymentValidation"></small>
                                        </div>
                                     </div>
                
                                            </div>
                                            <div class="col-lg-6">
                                                    <div  id="appointment_type">
                                        <div class="formLabel">
                                            <label for="right-label">
                                                Appointment Type <span class="mustFillin">*</span>
                                            </label>
                                        </div>
            
                                        <div class="formInput">
                                            <select id="appointment_type" name="appointment_type"><option value="0"> -- select -- </option><option>Full-Time</option><option>Part-Time</option><option>Contract</option></select> 
                                            <small id="appointmentValidation"></small>
                                        </div>
                                     </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <br>
                                        <div class="row">
                                                <div class="col-lg-6">
                                                        <div  id="employment_level">
                                             <div class="formLabel">
                                            <label for="right-label">
                                                Employment Level <span class="mustFillin">*</span>
                                            </label>
                                            </div>
            
                                            <div class="formInput">
                                            <select id="employment_level" name="employment_level"><option value="0"> -- select -- </option><option>Graduate</option><option>Junior</option><option>Intermediate</option><option>Senior</option></select> 
                                            <small id="employmentValidation"></small>
                                            </div>
                                             </div>
                    
                                                </div>
                                                <div class="col-lg-6">
                                                        <div  id="availability">
                                             <div class="formLabel">
                                            <label for="right-label">
                                                Availabilty <span class="mustFillin">*</span>
                                            </label>
                                            </div>
            
                                            <div class="formInput">
                                            <select id="availability" name="availability"><option value="0"> -- select -- </option><option>0-1 Week</option><option>2-3 Weeks</option><option>4-5 Weeks</option><option>Beyond 6 Weeks</option></select> 
                                            <small id="availabilityValidation"></small>
                                            </div>
                                             </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <br>
                                            <div class="row">
                                                    <div class="col-lg-6">
                                                            <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Latest Employment <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                                            <input id = "latest_employment" name="latest_employment" type="text" placeholder="Current Enterprise">
                                                    </div>

                                                    <div class="col-lg-6">
                                                            <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Years of experience <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                        <div class="formInput">
                                            <select id="years_of_experience" name="years_of_experience"><option value="0"> -- select -- </option><option>0</option><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option><option>13</option><option>14</option><option>15</option><option>16</option><option>17</option><option>18</option><option>19</option><option>20</option><option>21</option><option>22</option><option>23</option><option>24</option><option>25</option><option>26</option><option>27</option><option>28</option><option>29</option><option>30</option></select> 
                                            <small id="years_of_experienceValidation"></small>
                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                                <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Disabled <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                <input type="radio" name="disabled" value="No"> No<br>
                                <input type="radio" name="disabled" value="Yes"> Yes<br>
                        </div>
                        
                        <div class="col-lg-6">
                                <div class="formLabel">
                                            <label for="right-label" class="field-label">
                                                Disablility Description <span class="mustFillin">*</span>
                                            </label>
                                        </div>
                                <input class="form-input" type="text"   name="disability" placeholder="Disablility Description">
                                
                        </div>
                    </div>
                    <hr>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                                <h6>Linkedin Profile</h6> 
                                <input type="text" name="linkedin_profile" placeholder="Linkedin" >

                        </div>
                        <div class="col-lg-6">
                                <h6>Twitter Handle</h6>
                                <input type="text" name="twitter_handle" placeholder="Twitter" >
                        </div>
                    </div>
                    <br>
                    <br>                    
                    <div class="row">
                            <button type="submit" class="btn-primary" name="regbtn" id="regbtn" style="margin-left: 5%">Apply</button>

                    </div>
                    
                </div>

    </div>

</form>




</body>
</html> 