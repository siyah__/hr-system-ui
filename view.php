<!DOCTYPE html>
<html lang="en">
	<header>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PHP Upload File With Progressbar</title>
		
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style>
			.images{
				width:150px;
				height:150px;
				cursor:pointer;
				margin:10px;
			}
			.images:hover{
				-webkit-transform: scale(1.2);
				-moz-transform: scale(1.2);
				-o-transform: scale(1.2);
				transform: scale(1.2);
				transition: all 0.3s;
				-webkit-transition: all 0.3s;
			}
		</style>
	</header>
	<body>
		<div class="container">			
			<div class="page-header">
				<h1>Your Uploaded Files  </h1>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					
					<h3>Uploaded Files:</h3>
					<br/>

					<?php

					session_start();
						// Include the database configuration file
						// Database configuration
						$dbHost     = "localhost";
						$dbUsername = "root";
						$dbPassword = "";
						$dbName     = "hr_system";
						$email = $_GET['email'];
						// Create database connection
						$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

						// Check connection
						if ($db->connect_error) {
						    die("Connection failed: " . $db->connect_error);
						}
						
					 
						

						// Get images from the database
						$sql = "SELECT * FROM UserFiles where email = '$email';";

						$query = mysqli_query($db, $sql);

						//$query = $db->query($sql);

						if($query->num_rows > 0){
						    while($row = $query->fetch_assoc()){
						        $URL = $row["FilePath"]."/".$row["FileName"];
						?>
						    <a href="<?php echo $URL; ?>"><img src="<?php echo $URL; ?>" class="images" /></a>
						<?php }
						}else{ ?>
						    <p>No image(s) found...</p>
						<?php } 
					 ?>				
				</div>
			</div>
		</div>
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jQuery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>		
	</body>
</html>